# imperfect-product-card-exercise

This is in fulfillment of Imperfect Foods' Software Engineer assessment. 

The purpose of this repo is to serve as a API-powered single page application that displays a list of products and allows users to filter said products based on categroies.

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Setup

### Prerequisite

- node and npm installed
- node v10

### Installation

- clone this repo
- in your local version of this repo, run `npm ci`

### Development

- To run tests: `npm test`

- To run express API server: `nodemon api/server.js`

- To run app: `npm start`
- Open a web browser and go to `localhost:3000`

import axios from 'axios';

const client = axios.create({
  baseURL: 'http://localhost:9001',
  timeout: 5000,
})

export default client;

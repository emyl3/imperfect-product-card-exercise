import React from 'react';
import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import App from './App';
import Filters from './Filters'
import Products from './Products'

beforeAll(() => {
  configure({ adapter: new Adapter() });
})

describe('rendering', () => {
  let wrapper;

  it('renders a  <Filters /> component', () => {
    wrapper = shallow(<App />);
    expect(wrapper.find(Filters).length).toBe(1)
  });
  
  it('renders a <Products /> component', () => {
    wrapper = shallow(<App />);
    expect(wrapper.find(Products).length).toBe(1)
  });
})

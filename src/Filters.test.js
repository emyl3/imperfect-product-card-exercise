import React from 'react';
import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import Filters from './Filters'

beforeAll(() => {
  configure({ adapter: new Adapter() });
})

describe('rendering', () => {
  it('renders a  <Filters /> component', () => {
    let categories = [{
      id: '31442fe7-cf35-4bd5-888b-e28fcd6919af',
      name: 'Non-Produce'
    }, {
      id: '49a1f6a9-7115-4084-bf1f-b897f23cbeba',
      name: 'Bakery'
    }]

    let wrapper = shallow(<Filters categories={categories}/>);
    expect(wrapper.find('option').length).toBe(3);
    expect(wrapper.find('option').at(0).prop('value')).toEqual('all');
    expect(wrapper.find('option').at(1).prop('value')).toEqual('31442fe7-cf35-4bd5-888b-e28fcd6919af');
    expect(wrapper.find('option').at(2).prop('value')).toEqual('49a1f6a9-7115-4084-bf1f-b897f23cbeba');
  });
})

describe('rendering', () => {
  it('renders a  <Filters /> component', () => {
    let categories = [{
      id: '31442fe7-cf35-4bd5-888b-e28fcd6919af',
      name: 'Non-Produce'
    }, {
      id: '49a1f6a9-7115-4084-bf1f-b897f23cbeba',
      name: 'Bakery'
    }]

    let changeHandler = jest.fn();

    let wrapper = shallow(<Filters categories={categories} changeHandler={changeHandler}/>);
    wrapper.find('select').simulate('change', {target:  { value: '31442fe7-cf35-4bd5-888b-e28fcd6919af' }});
    expect(changeHandler).toBeCalledWith('31442fe7-cf35-4bd5-888b-e28fcd6919af')
  });
})


import React, { Component } from 'react';
import client from './axiosClient';

import './App.css';
import Products from './Products';
import Filters from './Filters';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      products: [],
      categories: []
    }
  }

  componentDidMount() {
    this.getProducts();
    this.getCategories();
  }

  getProducts = async (categoryId = null) => {
    let res = await client.get(`/products${categoryId === 'all' || categoryId === null ? '' : '/categories/' + categoryId}`);
    this.setState({ products: res.data });
  }

  getCategories = async () => {
    let res = await client.get('/products/categories');
    this.setState({ categories: res.data });
  }

  changeHandler = (categoryId) => {
    this.getProducts(categoryId)
  }

  render() {
    return (
      <div className="app">
      <div className="App-row">
        <Filters 
          categories={this.state.categories}
          changeHandler={this.changeHandler}/>
      </div>
      <div className="App-row">
        <Products products={this.state.products} />
      </div>
    </div>
    )
  }
}

export default App;

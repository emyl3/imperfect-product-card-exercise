import React from 'react';
import './Filters.css';

const Filters = ({ categories, changeHandler }) => {
  return (
    <div className="Filters-row">
      <select onChange={(event) => changeHandler(event.target.value)}>
        <option value="all">All Products</option>
        {
          categories.map((category) => 
            <option key={category.id} value={category.id}>{category.name}</option>)
        }
      </select>
    </div>
  )
}

export default Filters;

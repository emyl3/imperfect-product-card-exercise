const express = require('express');
const products = require('../src/products.json');

const app = express();
const port = process.env.PORT || 9001;

app.get('/products', (req, res) => {
  // TODO: actual call to DB
  return res.json(products);
});

app.get('/products/categories', (req, res) => {
  // TODO: actual call to DB
  return res.json(_getCategories(products))
});

app.get('/products/categories/:categoryId', (req, res) => {
  // TODO: actual call to DB
  const categoryId = req.params.categoryId
  return res.json(_filterCategories(categoryId, products))
});

app.listen(port, () => console.info(`App started, listening on ${port}`));

function _getCategories (products) {
  const categories = [];
  for (let product of products) {
    for (let [key, category] of Object.entries(product.categories)) {
      if (!categories.some(cat => cat.name === category.name)) {
        let categoryObj = {};
        categoryObj.name = category.name;
        categoryObj.id = category.categoryId;
        categories.push(categoryObj)
      }
    }
  }
  return categories;
}

function _filterCategories (categoryId, products) {
  const filteredProducts = [];
  for (let product of products) {
    if (product.categories.some(cat => cat.categoryId === categoryId)) {
      filteredProducts.push(product)
    }
  }
  return filteredProducts;
}
